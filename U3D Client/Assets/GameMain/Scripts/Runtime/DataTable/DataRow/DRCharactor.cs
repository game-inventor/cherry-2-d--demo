///--------------------------------------
///此文件由工具自动生成请不要改动
///--------------------------------------
using System;
using System.Collections.Generic;
using UnityGameFramework.Runtime;

namespace Cherry
{
	public class DRCharactor : DataRowBase
	{
		private int m_Id = 0;

		/// <summary>
		/// 获取角色模板ID
		/// </summary>
		public override int Id
		{
			get { return m_Id; }
		}

		/// <summary>
		/// 获取角色名
		/// </summary>
		public string Name
		{
			private set;
			get;
		}

		/// <summary>
		/// 获取待机动画
		/// </summary>
		public string Anim_Idle
		{
			private set;
			get;
		}

		/// <summary>
		/// 获取移动动画
		/// </summary>
		public string Anim_Move
		{
			private set;
			get;
		}

		/// <summary>
		/// 获取跳跃动画
		/// </summary>
		public string Anim_Jump
		{
			private set;
			get;
		}

		/// <summary>
		/// 获取降落动画
		/// </summary>
		public string Anim_Fall
		{
			private set;
			get;
		}

		public override bool ParseDataRow(string dataRowString, object userData)
		{
			string[] columnStrings = dataRowString.Split(new char[] { '\t' });
			for (int i = 0; i < columnStrings.Length; i++)
			{
				columnStrings[i] = columnStrings[i].Trim(new char[] { '\"' });
			}
			int index = 0;
			m_Id = int.Parse(columnStrings[index++]);
			Name = columnStrings[index++];
			Anim_Idle = columnStrings[index++];
			Anim_Move = columnStrings[index++];
			Anim_Jump = columnStrings[index++];
			Anim_Fall = columnStrings[index++];
			return true;
		}
	}
}
