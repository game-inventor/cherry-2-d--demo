﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public partial class TimerHelper : IReference
	{
		/// <summary>
		/// 对象的单一计时器。
		/// </summary>
		private class Timer : IReference
		{
			private float m_DelayTime;                      //延时时间
			private float m_IntervalTime;                     //间隔时间
			private float m_LastTime;                           //上次间隔时间
			private float m_CurrentTime;                        //当前时间
			private int m_RepeatCount;                      //间隔次数
			private int m_CurrentCount;                     //当前间隔次数
			private bool m_IsEnable;                            //是否生效
			private bool m_IsValid;                             //是否有效
			private TimerCallback m_OnIntervalCall;     //间隔回调函数
			private TimerCallback m_OnCompleteCall; //完成回调函数

			/// <summary>
			/// Timer是否合法。
			/// </summary>
			public bool IsValid
			{
				get { return m_IsValid; }
				set
				{
					m_IsValid = value;
				}
			}

			public Timer()
			{
				m_DelayTime = 0;
				m_IntervalTime = 0;
				m_LastTime = 0;
				m_CurrentTime = 0;
				m_RepeatCount = 0;
				m_CurrentCount = 0;
				m_IsEnable = false;
				m_IsValid = false;
				m_OnIntervalCall = null;
				m_OnCompleteCall = null;
			}

			public void Update()
			{
				if (m_IsEnable)
				{
					TickTimer(Time.deltaTime);
				}
			}

			/// <summary>
			/// 启动计时器。
			/// </summary>
			public void Start()
			{
				m_IsEnable = true;
			}

			/// <summary>
			/// 初始化单次回调计时器。
			/// </summary>
			public static Timer CreateOnceTimer(float delayTime, bool start, TimerCallback onComplete)
			{
				return Create(delayTime, 0, 0, start, null, onComplete);
			}

			/// <summary>
			/// 初始化计时器。
			/// </summary>
			/// <param name="delayTime">延时时间。</param>
			/// <param name="intervalTime">间隔时间。</param>
			/// <param name="repeatCount">重复次数，小于或等于0时代表无限。</param>
			/// <param name="start">是否初始化后启动计时器。</param>
			/// <param name="onInterval">间隔回调函数。</param>
			/// <param name="onComplete">结束回调函数。</param>
			public static Timer Create(float delayTime, float intervalTime, int repeatCount, bool start, TimerCallback onInterval, TimerCallback onComplete)
			{
				Timer timer = ReferencePool.Acquire<Timer>();
				timer.m_DelayTime = delayTime;
				timer.m_IntervalTime = intervalTime;
				timer.m_RepeatCount = repeatCount;
				timer.m_IsEnable = start;
				timer.m_IsValid = true;
				timer.m_OnIntervalCall = onInterval;
				timer.m_OnCompleteCall = onComplete;

				return timer;
			}

			public void Clear()
			{
				m_DelayTime = 0;
				m_IntervalTime = 0;
				m_LastTime = 0;
				m_CurrentTime = 0;
				m_RepeatCount = 0;
				m_CurrentCount = 0;
				m_IsEnable = false;
				m_IsValid = false;
				m_OnIntervalCall = null;
				m_OnCompleteCall = null;
			}

			private void TickTimer(float deltaTime)
			{
				m_CurrentTime += deltaTime;
				if (m_CurrentTime < m_DelayTime)
				{
					return;
				}

				//延时结束，从现在开始算间隔时间。
				if (m_CurrentCount == 0)
				{
					m_LastTime = m_DelayTime;
				}

				if (m_CurrentTime - m_LastTime > m_IntervalTime)
				{
					//无限重复
					if (m_RepeatCount <= 0)
					{
						m_CurrentCount++;
						m_LastTime = m_CurrentCount;
						m_OnIntervalCall?.Invoke();
					}
					else
					{
						if (m_CurrentCount < m_RepeatCount)
						{
							m_CurrentCount++;
							m_LastTime = m_CurrentCount;
							m_OnIntervalCall?.Invoke();
						}
					}
				}

				if (m_CurrentCount >= m_RepeatCount)
				{
					m_OnCompleteCall?.Invoke();
					//TODO 添加一个是否自动销毁的字段。
					m_IsValid = false;
					m_IsEnable = false;
				}
			}
		}
	}


}

