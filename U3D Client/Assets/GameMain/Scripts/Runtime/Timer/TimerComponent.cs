﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace Cherry
{
	/// <summary>
	/// 游戏计时器组件。
	/// </summary>
	public class TimerComponent : GameFrameworkComponent
	{
		private TimerManager m_TimerManager;

		protected override void Awake()
		{
			base.Awake();
		}

		// Start is called before the first frame update
		void Start()
		{
			m_TimerManager = new TimerManager();
		}

		// Update is called once per frame
		void Update()
		{
			m_TimerManager.Update();
		}

		/// <summary>
		/// 创建计时器辅助器。
		/// </summary>
		/// <param name="master">计时器辅助器持有者。</param>
		/// <returns>计时器辅助器。</returns>
		public TimerHelper CreateTimeHelper(object master)
		{
			return m_TimerManager.CreateTimeHelper(master);
		}

		/// <summary>
		/// 移除计时器辅助器。
		/// </summary>
		/// <param name="master">计时器辅助器持有者。</param>
		public void RemoveTimeHelper(object master)
		{
			m_TimerManager.RemoveTimeHelper(master);
		}

		/// <summary>
		/// 移除所有计时器辅助器。
		/// </summary>
		public void RemoveAllTimeHelper()
		{
			m_TimerManager.RemoveAllTimeHelper();
		}
	}
}

