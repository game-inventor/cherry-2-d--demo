﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	/// <summary>
	/// 对象计时器。
	/// </summary>
	public partial class TimerHelper : IReference
	{
		private Dictionary<string, Timer> m_Timers;
		private List<string> m_InvalidList;
		private bool m_IsValid;

		public bool IsValid
		{
			get { return m_IsValid; }
			set { m_IsValid = value; }
		}

		public static TimerHelper Create()
		{
			TimerHelper timer = ReferencePool.Acquire<TimerHelper>();
			timer.m_Timers = new Dictionary<string, Timer>();
			timer.m_InvalidList = new List<string>();
			timer.m_IsValid = true;

			return timer;
		}


		public void Update()
		{
			if (m_Timers.Count > 0)
			{
				//先进行Timer的轮询，防止当前帧逻辑失效。
				foreach (var timer in m_Timers)
				{
					timer.Value.Update();
				}

				//Timer回收
				foreach (var timer in m_Timers)
				{
					if (!timer.Value.IsValid)
					{
						m_InvalidList.Add(timer.Key);
					}
				}

				foreach (var removeStr in m_InvalidList)
				{
					m_Timers.TryGetValue(removeStr, out Timer timer);
					ReferencePool.Release(timer);
					m_Timers.Remove(removeStr);
				}
				m_InvalidList.Clear();
			}
		}

		public void StartTimer(string timerName)
		{
			Timer timer = InternalGetTimer(timerName);
			if (timer == null)
				return;

			timer.Start();
		}

		/// <summary>
		/// 初始化单次回调计时器。
		/// </summary>
		public void CreateOnceTimer(string name ,float delayTime, bool start, TimerCallback onComplete)
		{
			CreateTimer(name, delayTime, 0, 0, start, null, onComplete);
		}

		/// <summary>
		/// 初始化计时器。
		/// </summary>
		/// <param name="name">计时器名称。</param>
		/// <param name="delayTime">延时时间。</param>
		/// <param name="intervalTime">间隔时间。</param>
		/// <param name="repeatCount">重复次数，小于或等于0时代表无限。</param>
		/// <param name="start">是否初始化后启动计时器。</param>
		/// <param name="onInterval">间隔回调函数。</param>
		/// <param name="onComplete">结束回调函数。</param>
		/// <returns></returns>
		public void CreateTimer(string name, float delayTime, float intervalTime, int repeatCount, bool start, TimerCallback onInterval, TimerCallback onComplete)
		{
			Timer newTimer = null;
			if (!m_Timers.TryGetValue(name,out Timer timer))
			{
				newTimer = Timer.Create(delayTime, intervalTime, repeatCount, start, onInterval, onComplete);
				m_Timers.Add(name, newTimer);
			}
			else
			{
				GLogger.ErrorFormat(Log_Channel.Error, "不能添加该Timer！Timer name已存在！Timer name:{0}", name);
			}
		}

		public void RemoveTimer(string name)
		{
			if (m_Timers.TryGetValue(name,out Timer timer))
			{
				ReferencePool.Release(timer);
				m_IsValid = false;
			}
			else
			{
				GLogger.ErrorFormat(Log_Channel.Error, "移除Timer失败！该Timer name不存在！Timer name:{0}", name);
			}
		}

		/// <summary>
		/// 移除TimerHelper以及它拥有的所有Timer。
		/// </summary>
		public void RemoveAllTimer()
		{
			foreach (var timer in m_Timers)
			{
				timer.Value.IsValid = false;
			}

			m_IsValid = false;
		}
		
		public void Clear()
		{
			m_Timers.Clear();
			m_InvalidList.Clear();
		}

		private Timer InternalGetTimer(string timerName)
		{
			Timer timer = null;

			if (timerName == null || timerName == "")
			{
				GLogger.Error(Log_Channel.NoneReference, "params#1 name is null.");
				return timer;
			}

			if (!m_Timers.TryGetValue(timerName,out timer))
			{
				GLogger.ErrorFormat(Log_Channel.Error, "没有这个名字的Timer！name:{0}", timerName);
				return timer;
			}
			else
			{
				return timer;
			}
		}
	}
}

