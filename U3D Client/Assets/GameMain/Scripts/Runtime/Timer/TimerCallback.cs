﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	/// <summary>
	/// 计时器完成事件。
	/// </summary>
	/// <param name="timer"></param>
	public delegate void TimerCallback();
}

