﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	/// <summary>
	/// 计时器管理器。
	/// </summary>
	public class TimerManager
	{
		private Dictionary<object, TimerHelper> m_TimeHelpers;
		private List<object> m_InValidList = new List<object>();

		public TimerManager()
		{
			m_TimeHelpers = new Dictionary<object, TimerHelper>();
			m_InValidList = new List<object>();
		}

		public void Update()
		{
			if (m_TimeHelpers.Count > 0)
			{
				//先执行TimerHelper的轮询，包含逻辑以及回收，避免Timer逻辑在当前帧失效。
				foreach (var timeHelper in m_TimeHelpers)
				{
					timeHelper.Value.Update();
				}

				//在进行回收
				foreach (var timer in m_TimeHelpers)
				{
					if (!timer.Value.IsValid)
					{
						m_InValidList.Add(timer.Key);
					}
				}

				foreach (var removeObj in m_InValidList)
				{
					m_TimeHelpers.TryGetValue(removeObj, out TimerHelper timeHelper);
					ReferencePool.Release(timeHelper);
					m_TimeHelpers.Remove(removeObj);
				}
				m_InValidList.Clear();
			}
		}

		/// <summary>
		/// 创建计时器辅助器。
		/// </summary>
		/// <param name="master">计时器辅助器持有者。</param>
		/// <returns>计时器辅助器。</returns>
		public TimerHelper CreateTimeHelper(object master)
		{
			TimerHelper newHelper = null;

			if (m_TimeHelpers.TryGetValue(master,out TimerHelper helper))
			{
				GLogger.DebugFormat(Log_Channel.Error, "不能为一个object创建重复的TimeHelper！object name:{0}", master.ToString());
			}
			else
			{
				newHelper = TimerHelper.Create();
				m_TimeHelpers.Add(master, newHelper);
			}

			return newHelper;
		}

		/// <summary>
		/// 移除计时器辅助器。
		/// </summary>
		/// <param name="master">计时器辅助器持有者。</param>
		public void RemoveTimeHelper(object master)
		{
			if (master == null)
			{
				GLogger.Error(Log_Channel.Error, "master is null!");
				return;
			}

			if (!m_TimeHelpers.TryGetValue(master, out TimerHelper helper))
			{
				GLogger.DebugFormat(Log_Channel.Error, "该object没有可移除的TimeHelper！object name:{0}", master.ToString());
			}
			else
			{
				ReferencePool.Release(helper);
				m_TimeHelpers.Remove(master);
			}
		}

		/// <summary>
		/// 移除所有计时器辅助器。
		/// </summary>
		public void RemoveAllTimeHelper()
		{
			foreach (var timeHelper in m_TimeHelpers)
			{
				timeHelper.Value.IsValid = false;
			}

			m_TimeHelpers.Clear();
		}
	}

}
