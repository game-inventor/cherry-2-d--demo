﻿using GameFramework.Event;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace Cherry
{
	public class LoadingUIForm : UGUIFormBase
	{
		TimerHelper m_TimeHelper = null;

		protected override void OnInit(object userData)
		{
			base.OnInit(userData);
		}

		protected override void OnOpen(object userData)
		{
			base.OnOpen(userData);

			GameEntry.Input.SetInputKeyValid(false, InputType.GetKey, KeyCode.LeftArrow);
			GameEntry.Input.SetInputKeyValid(false, InputType.GetKey, KeyCode.RightArrow);
			GameEntry.Input.SetInputKeyValid(false, InputType.GetKeyDown, KeyCode.Space);

			if (userData.ToString() == "init")
			{
				//初始化加载界面对象不回收
				InternalSetVisible(false);
				GameEntry.UI.SetUIFormInstanceLocked(UIForm, true);
				Close();
				return;
			}
			else
			{

				//GameEntry.Event.Subscribe(LoadSceneSuccessEventArgs.EventId, OnLoadSceneSuccess);
			}

			InitTimer();

			GLogger.Debug(Log_Channel.UI, "打开loading界面");
		}

		protected override void OnClose(bool isShutdown, object userData)
		{
			base.OnClose(isShutdown, userData);

			GameEntry.Input.SetInputKeyValid(true, InputType.GetKey, KeyCode.LeftArrow);
			GameEntry.Input.SetInputKeyValid(true, InputType.GetKey, KeyCode.RightArrow);
			GameEntry.Input.SetInputKeyValid(true, InputType.GetKeyDown, KeyCode.Space);

			if (m_TimeHelper != null)
			{
				m_TimeHelper.RemoveAllTimer();
			}

			GLogger.Debug(Log_Channel.UI, "关闭loading界面");
		}

		private void OnLoadSceneSuccess(object sender, GameEventArgs e)
		{
			GameEntry.UI.CloseUIFormByUIForm(this);
			GameEntry.Event.Unsubscribe(LoadSceneSuccessEventArgs.EventId, OnLoadSceneSuccess);
		}

		private void InitTimer()
		{
			if (m_TimeHelper == null)
			{
				m_TimeHelper = GameEntry.Timer.CreateTimeHelper(this);
			}
			m_TimeHelper.CreateOnceTimer("CloseLoadingUI", 10, false, OnCloseLoadingUI);
			m_TimeHelper.StartTimer("CloseLoadingUI");
		}

		private void OnCloseLoadingUI()
		{
			Close();
		}
	}
}
