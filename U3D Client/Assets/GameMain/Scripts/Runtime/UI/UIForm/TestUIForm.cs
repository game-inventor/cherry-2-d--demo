﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameFramework.DataTable;
using Cherry.Tilemaps;
using UnityEngine.Tilemaps;

namespace Cherry
{
	public class TestUIForm : UGUIFormBase
	{
		protected override void OnOpen(object userData)
		{
			base.OnOpen(userData);
			UnityGameFramework.Runtime.Log.Info("打开主界面");
		}

		public override void OnClick(GameObject gameObject)
		{
			//load scene
			if (gameObject.name == "Button")
			{
				GameEntry.UI.CloseUIForm(UIForm);
				//GameEntry.Resource.LoadAsset("Assets/GameMain/AssetData/Map/DarkForest/Forest.asset", new GameFramework.Resource.LoadAssetCallbacks(LoadSuccess));
				//GameEntry.Map.LoadChunk("Assets/GameMain/AssetData/Map/DarkForest/Forest.asset");

				//TODO 发送进入GameMain流程事件
				GameEntry.Event.Fire(this, new CommonEventArgs(ClientEvent.EnterGameMainProcedure));
				TestLoad();

			}
		}


		//测试加载地图，不论是否真正加载了内存，加载成功回调都会调用
		private void LoadSuccess(string chunkAssetName, object chunkAsset, float duration, object userData)
		{
			Debug.LogError(1);
			this.StartCoroutine(LoadTilemap((TilemapData)chunkAsset));
		}

		private IEnumerator LoadTilemap(TilemapData chunkAsset)
		{
			GameObject grid = GameObject.Find("Grid");
			if (grid == null)
			{
				grid = new GameObject("Grid");
			}
			grid.AddComponent<Grid>();
			grid.transform.position = new Vector3(-0.5f, -0.5f, 0);
			GameObject map = new GameObject(chunkAsset.TilemapName);
			map.transform.SetParent(grid.transform);
			Tilemap tilemap = map.AddComponent<Tilemap>();
			tilemap.transform.localPosition = Vector3.zero;
			map.AddComponent<TilemapRenderer>();
			foreach (var item in chunkAsset.TileInfoList)
			{
				tilemap.SetTile(item.IntPos, (CherryTile)item.Tile);
				yield return new WaitForEndOfFrame();
			}
			yield return null;
		}

		private void TestLoad()
		{
			GameEntry.Resource.LoadAssetList(new List<string> {
				"Assets/GameMain/AssetData/Map/DarkForest/Forest.asset",
				"Assets/GameMain/AssetData/Map/DarkCountry/Country.asset",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_1_BG.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_2_BG.png",
				"Assets/Artwork/Sprite/1_Scene_Level_1-1/props/tileset.png",
				"Assets/GameMain/AssetData/Map/DarkForest/Forest.asset",
				"Assets/GameMain/AssetData/Map/DarkCountry/Country.asset",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_1_BG.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_2_BG.png",
				"Assets/Artwork/Sprite/1_Scene_Level_1-1/props/tileset.png",
				"Assets/GameMain/AssetData/Map/DarkForest/Forest.asset",
				"Assets/GameMain/AssetData/Map/DarkCountry/Country.asset",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_1_BG.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_2_BG.png",
				"Assets/Artwork/Sprite/1_Scene_Level_1-1/props/tileset.png",
				"Assets/GameMain/AssetData/Map/DarkForest/Forest.asset",
				"Assets/GameMain/AssetData/Map/DarkCountry/Country.asset",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_1_BG.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_2_BG.png",
				"Assets/Artwork/Sprite/1_Scene_Level_1-1/props/tileset.png",
				"Assets/GameMain/AssetData/Map/DarkForest/Forest.asset",
				"Assets/GameMain/AssetData/Map/DarkCountry/Country.asset",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level1-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-2.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level2-3.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_1_BG.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LevelSelect/Level_2_BG.png",
				"Assets/Artwork/Sprite/1_Scene_Level_1-1/props/tileset.png",
				"Assets/GameMain/AssetData/Map/DarkForest/Forst.asset",
				"Assets/GameMain/AssetData/Map/DarkCountry/Contry.asset",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/LeveSelect/Level1-1.png",
				"Assets/Artwork/Sprite/0_Scene_Start&Level/Leveelect/Level1-2.png",
				}
			,new GameFramework.Resource.LoadAssetCallbacks(OnLoadAssetSuccess,OnLoadAssetFailure)
			,new LoadAssetListCallbacks(OnLoadAssetListSuccess,OnLoadAssetListFailure),1);
		}

		private void OnLoadAssetSuccess(string assetName,object asset,float duration, object userData)
		{
			GLogger.DebugFormat(Log_Channel.Resource, "加载单个资源成功，assetName:{0},asset type:{1},time:{2},userData:{3}", assetName, asset.GetType().ToString(), duration, userData?.ToString());
		}

		private void OnLoadAssetListSuccess(List<string> assetNames, List<object> assets, float duration, object userData)
		{
			for (int i = 0; i < assetNames.Count; i++)
			{
				GLogger.DebugFormat(Log_Channel.Resource, "-----------加载资源组成功，assetName:{0},asset type:{1},time:{2},userData:{3}", assetNames[i], assets[i].GetType().ToString(), duration, userData?.ToString());
			}
		}

		private void OnLoadAssetFailure(string assetName, GameFramework.Resource.LoadResourceStatus status,string errorMessage,object userData)
		{
			GLogger.DebugFormat(Log_Channel.Resource, "加载单个资源失败，assetName:{0},status type:{1},errorMessage:{2},userData:{3}", assetName, status.ToString(), errorMessage, userData?.ToString());
		}

		private void OnLoadAssetListFailure(List<string> assetNames, List<GameFramework.Resource.LoadResourceStatus> statuses, List<string> errorMessages, object userData)
		{
			for (int i = 0; i < assetNames.Count; i++)
			{
				GLogger.DebugFormat(Log_Channel.Resource, "-----------加载资源组失败，assetName:{0},status type:{1},errorMessage:{2},userData:{3}", assetNames[i], statuses[i].ToString(), errorMessages[i], userData?.ToString());
			}
		}
	}
}
