﻿using GameFramework.Event;
using GameFramework.Fsm;
using GameFramework.Procedure;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	/// <summary>
	/// 游戏主流程。
	/// </summary>
	public class ProcedureGameMain : ProcedureBase
	{
		protected override void OnInit(IFsm<IProcedureManager> procedureOwner)
		{
			base.OnInit(procedureOwner);
		}

		protected override void OnEnter(IFsm<IProcedureManager> procedureOwner)
		{
			base.OnEnter(procedureOwner);

			GameEntry.Event.Subscribe(ChunkLoadedEventArgs.EventId, OnChunkLoaded);

			//1.读取玩家存档，初始化玩家数据
			GLogger.Debug(Log_Channel.Log, "玩家数据初始化！");
			//2.加载地图 TODO打开Loading界面
			GameEntry.Map.LoadChunk(ChunkType.Forest);
			GameEntry.UI.OpenUIFormByUIFormType(UIFormType.LoadingUIForm);
		}

		protected override void OnLeave(IFsm<IProcedureManager> procedureOwner, bool isShutdown)
		{
			base.OnLeave(procedureOwner, isShutdown);
		}

		private void OnChunkLoaded(object sender, GameEventArgs eventArgs)
		{
			//加载完成后设置玩家位置
			PlayerData data = PlayerData.Create(1,new Vector3(1,2,1),Quaternion.identity);
			GameEntry.Entity.ShowEntity<Player>("Assets/GameMain/AssetData/Player/Player.prefab", "Player", data);
		}
	}
}

