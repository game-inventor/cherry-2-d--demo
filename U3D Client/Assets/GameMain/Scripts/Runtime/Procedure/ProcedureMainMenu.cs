﻿using GameFramework.Event;
using GameFramework.Fsm;
using GameFramework.Procedure;
using UnityEngine;

namespace Cherry
{
	/// <summary>
	/// 主菜单流程。
	/// </summary>
	public class ProcedureMainMenu : ProcedureBase
	{
		private bool m_CanEnterGameMain = false;

		protected override void OnInit(IFsm<IProcedureManager> procedureOwner)
		{
			base.OnInit(procedureOwner);
		}

		protected override void OnEnter(IFsm<IProcedureManager> procedureOwner)
		{
			base.OnEnter(procedureOwner);
			UnityGameFramework.Runtime.Log.Info("进入游戏主流程");

			GameEntry.Event.Subscribe(CommonEventArgs.EventId, OnCommonEvent);

			//打开主界面
			GameEntry.UI.OpenUIFormByUIFormType(UIFormType.TestUIForm, null);
			//测试用弹出界面
			//GameEntry.UI.OpenUIForm("Assets/GameMain/AssetData/UI/Image.prefab", "Top");
		}

		protected override void OnUpdate(IFsm<IProcedureManager> procedureOwner, float elapseSeconds, float realElapseSeconds)
		{
			base.OnUpdate(procedureOwner, elapseSeconds, realElapseSeconds);
			if (m_CanEnterGameMain)
			{
				ChangeState<ProcedureGameMain>(procedureOwner);
			}
		}

		protected override void OnLeave(IFsm<IProcedureManager> procedureOwner, bool isShutdown)
		{
			base.OnLeave(procedureOwner, isShutdown);
		}

		protected override void OnDestroy(IFsm<IProcedureManager> procedureOwner)
		{
			base.OnDestroy(procedureOwner);
		}

		private void OnCommonEvent(object sender, GameEventArgs eventArgs)
		{
			CommonEventArgs Args = eventArgs as CommonEventArgs;

			if (Args.ClientEventType == (int)ClientEvent.EnterGameMainProcedure)
			{
				m_CanEnterGameMain = true;
			}
		}
	}
}
