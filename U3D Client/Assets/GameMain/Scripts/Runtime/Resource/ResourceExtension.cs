﻿using GameFramework.Resource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace Cherry
{
	public static class ResourceExtension
	{
		public static void LoadAssetList(this ResourceComponent component, List<string> assetNames,LoadAssetCallbacks loadAssetCallbacks,LoadAssetListCallbacks loadAssetListCallbacks, object userData)
		{
			//形成闭包
			int finishCount = 0;
			float startTime = Time.time;
			bool isSucceed = true;
			List<string> loadedAssetNames = new List<string>();
			List<object> loadedAssets = new List<object>();
			List<string> failLoadedAssetNames = new List<string>();
			List<LoadResourceStatus> failLoadedStatuses = new List<LoadResourceStatus>();
			List<string> failLoadedMessages = new List<string>();
			LoadAssetCallbacks internalLoadAssetCallbacks = new LoadAssetCallbacks(
				//Delegate Type LoadAssetSuccessCallback
				(string assetName, object asset, float duration, object internalUserData) =>
				{
					loadAssetCallbacks.LoadAssetSuccessCallback?.Invoke(assetName, asset, duration, internalUserData);
					loadedAssetNames.Add(assetName);
					loadedAssets.Add(asset);
					finishCount++;
					Debug.Log(finishCount);

					if (finishCount == assetNames.Count)
					{
						if (isSucceed)
						{
							float totalDuration = Time.time - startTime;
							loadAssetListCallbacks.LoadAssetListSuccessCallBack?.Invoke(loadedAssetNames, loadedAssets, totalDuration, internalUserData);
						}
						else
						{
							loadAssetListCallbacks.LoadAssetListFailureCallback?.Invoke(failLoadedAssetNames, failLoadedStatuses, failLoadedMessages, internalUserData);
						}
					}

				},
				//Delegate Type LoadAssetFailureCallback
				(string assetName, LoadResourceStatus status, string errorMessage, object internalUserData) =>
				{
					loadAssetCallbacks.LoadAssetFailureCallback?.Invoke(assetName, status, errorMessage, internalUserData);
					finishCount++;
					failLoadedAssetNames.Add(assetName);
					failLoadedStatuses.Add(status);
					failLoadedMessages.Add(errorMessage);
					isSucceed = false;

					if (finishCount == assetNames.Count && isSucceed == false)
					{
						Debug.Log("资源组加载失败！");
						loadAssetListCallbacks.LoadAssetListFailureCallback?.Invoke(failLoadedAssetNames, failLoadedStatuses, failLoadedMessages, internalUserData);
					}
				}
				);

			foreach (var assetName in assetNames)
			{
				component.LoadAsset(assetName, internalLoadAssetCallbacks, userData);
			}
		}
	}
}

