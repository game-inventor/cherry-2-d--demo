﻿using GameFramework.Resource;
using System.Collections.Generic;

namespace Cherry
{
	public delegate void LoadAssetListFailureCallback(List<string> assetNames, List<LoadResourceStatus> statuses, List<string> errerMessages, object userData);
}


