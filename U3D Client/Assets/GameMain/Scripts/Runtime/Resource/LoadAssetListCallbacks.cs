﻿namespace Cherry
{
	public class LoadAssetListCallbacks
	{
		private readonly LoadAssetListSuccessCallback m_LoadAssetListSuccessCallback;
		private readonly LoadAssetListFailureCallback m_LoadAssetListFailureCallback;

		public LoadAssetListCallbacks(LoadAssetListSuccessCallback loadAssetListSuccessCallback)
		{
			m_LoadAssetListSuccessCallback = loadAssetListSuccessCallback;
			m_LoadAssetListFailureCallback = null;
		}

		public LoadAssetListCallbacks(LoadAssetListSuccessCallback loadAssetListSuccessCallback, LoadAssetListFailureCallback loadAssetListFailureCallback)
		{
			m_LoadAssetListSuccessCallback = loadAssetListSuccessCallback;
			m_LoadAssetListFailureCallback = loadAssetListFailureCallback;
		}

		public LoadAssetListSuccessCallback LoadAssetListSuccessCallBack
		{
			get { return m_LoadAssetListSuccessCallback; }
		}

		public LoadAssetListFailureCallback LoadAssetListFailureCallback
		{
			get { return m_LoadAssetListFailureCallback; }
		}
	}
}

