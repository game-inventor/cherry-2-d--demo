﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	/// <summary>
	/// 用于放在GameObject上，预加载动画的组件。
	/// </summary>
	public class AnimationPreloader : MonoBehaviour
	{
		public List<AnimationClip> AnimationClips;

		// Start is called before the first frame update
		void Start()
		{

		}
	}
}

