﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Animations;

namespace Cherry
{
	public class SampleAnimController
	{
		private Dictionary<string, int> m_ClipName2Index;
		private PlayableGraph m_AnimGraph;
		private PlayableOutput m_PlayableOutput;
		private AnimationMixerPlayable m_Mixer;
		private Animator m_Animator;
		private int m_CurrentIndex = -1;

		public SampleAnimController(Animator animator)
		{
			m_Animator = animator;
			m_ClipName2Index = new Dictionary<string, int>();
		}

		public void Init(List<AnimationClip> clips, string name)
		{
			m_AnimGraph = PlayableGraph.Create("Man_Graph");
			m_PlayableOutput = AnimationPlayableOutput.Create(m_AnimGraph, name, m_Animator);
			m_Mixer = AnimationMixerPlayable.Create(m_AnimGraph, clips.Count, true);
			m_PlayableOutput.SetSourcePlayable(m_Mixer);
			for (int i = 0; i < clips.Count; i++)
			{
				AnimationClipPlayable playable = AnimationClipPlayable.Create(m_AnimGraph, clips[i]);
				m_ClipName2Index.Add(clips[i].name, i);
				m_AnimGraph.Connect(playable, 0, m_Mixer, i);
			}
		}

		public void PlayAnimation(string clipName)
		{
			if (!m_AnimGraph.IsPlaying())
			{
				m_AnimGraph.Play();
			}

			int index;
			if (m_ClipName2Index.TryGetValue(clipName, out index))
			{
				if (m_CurrentIndex != -1)
				{
					m_Mixer.SetInputWeight(m_CurrentIndex, 0f);
				}
				
				m_CurrentIndex = index;
				m_Mixer.SetInputWeight(index, 1f);
			}
			else
			{
				GLogger.ErrorFormat(Log_Channel.Animation, "不存在AnimationClip name为{0}的动画！请检查是否配置正确的动画名！", clipName);
			}
		}

		public bool IsPlayingAnimation(string clipName)
		{
			if (!m_AnimGraph.IsPlaying())
			{
				return false;
			}

			if (m_Mixer.GetInputWeight(m_CurrentIndex) == 1.0f)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public void Destroy()
		{
			m_AnimGraph.Destroy();
		}
	}
}
