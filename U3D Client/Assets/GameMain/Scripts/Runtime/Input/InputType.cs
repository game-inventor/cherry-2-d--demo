﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public enum InputType
	{
		None,
		GetKey,
		GetKeyDown,
		GetKeyUp,
	}
}

