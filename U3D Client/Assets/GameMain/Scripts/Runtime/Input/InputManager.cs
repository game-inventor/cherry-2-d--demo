﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public partial class InputManager
	{
		private Dictionary<KeyCode, InputData> m_InputKeyValues;
		private Dictionary<KeyCode, InputData> m_InputKeyDownValues;
		private Dictionary<KeyCode, InputData> m_InputKeyUpValues;

		public InputManager()
		{
			m_InputKeyValues = new Dictionary<KeyCode, InputData>();
			m_InputKeyDownValues = new Dictionary<KeyCode, InputData>();
			m_InputKeyUpValues = new Dictionary<KeyCode, InputData>();
		}

		public void Update()
		{
			//监听键盘按下的输入
			AddGetKeyDownValue2Dic(KeyCode.LeftArrow);
			AddGetKeyDownValue2Dic(KeyCode.RightArrow);
			AddGetKeyDownValue2Dic(KeyCode.Space);


			//监听持续键入的输入
			AddGetKeyValue2Dic(KeyCode.LeftArrow);
			AddGetKeyValue2Dic(KeyCode.RightArrow);

			//监听键盘释放的输入




			//测试功能
			InputData data = GetInputKeyDataByKeyCode(InputType.GetKeyDown, KeyCode.Space);
			if (data != null)
			{
				if (data.Value == 1)
				{

					if (Time.time - data.LastTime < 1)
					{
						GLogger.Debug(Log_Channel.Input, "Space twice in one sceonds!");
					}
					else
					{
						GLogger.Debug(Log_Channel.Input, "Space");
					}
				}
			}
		}

		/// <summary>
		/// 设置键盘输入合法性。
		/// </summary>
		/// <param name="isValid">是否合法。</param>
		/// <param name="inputType">键盘输入类型。（按下，按住，松开）</param>
		/// <param name="keyCode">输入键类型。</param>
		public void SetInputKeyValid(bool isValid, InputType inputType, KeyCode keyCode)
		{
			InputData data = GetInputKeyDataByKeyCode(inputType, keyCode);
			if (data == null)
			{
				GLogger.ErrorFormat(Log_Channel.Input, "按键数据不存在！Input type:{0},key code:{1}", inputType.ToString(), keyCode.ToString());
			}

			data.SetDataValid(isValid);
		}

		/// <summary>
		/// 键盘输入是否存在。
		/// </summary>
		/// <param name="inputType">键盘输入类型。（按下，按住，松开）</param>
		/// <param name="keyCode">输入键类型。</param>
		public bool IsInputKeyExist(InputType inputType, KeyCode keyCode)
		{
			InputData data = GetInputKeyDataByKeyCode(inputType, keyCode);
			if (data == null)
			{
				return false;
			}

			if (data.IsValid && data.Value == 1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public InputData GetInputKeyDataByKeyCode(InputType inputType, KeyCode keyCode)
		{
			switch (inputType)
			{
				case InputType.GetKey:
					if (!m_InputKeyValues.TryGetValue(keyCode, out InputData keyData))
					{
						GLogger.ErrorFormat(Log_Channel.Input, "没有找到对应KeyCode的InputData，KeyCode name：{0}", keyCode.ToString());
						return null;
					}

					return keyData;
				case InputType.GetKeyDown:
					if (!m_InputKeyDownValues.TryGetValue(keyCode, out InputData keyDownData))
					{
						GLogger.ErrorFormat(Log_Channel.Input, "没有找到对应KeyCode的InputData，KeyCode name：{0}", keyCode.ToString());
						return null;
					}

					return keyDownData;
				case InputType.GetKeyUp:
					if (!m_InputKeyUpValues.TryGetValue(keyCode, out InputData keyUpData))
					{
						GLogger.ErrorFormat(Log_Channel.Input, "没有找到对应KeyCode的InputData，KeyCode name：{0}", keyCode.ToString());
						return null;
					}

					return keyUpData;
				default:
					GLogger.ErrorFormat(Log_Channel.Input, "无效的inputType，inputType name：{0}", inputType.ToString());
					return null;
			}
		}

		private void AddGetKeyValue2Dic(KeyCode inputType)
		{
			if (Input.GetKey(inputType))
			{
				ExecuteInputData(m_InputKeyValues, inputType, InputType.GetKey, 1);
			}
			else
			{
				ExecuteInputData(m_InputKeyValues, inputType, InputType.GetKey, 0);
			}
		}

		private void AddGetKeyDownValue2Dic(KeyCode inputType)
		{
			if (Input.GetKeyDown(inputType))
			{
				ExecuteInputData(m_InputKeyDownValues, inputType, InputType.GetKeyDown, 1);
			}
			else
			{
				ExecuteInputData(m_InputKeyDownValues, inputType, InputType.GetKeyDown, 0);
			}
		}

		private void AddGetKeyUpValue2Dic(KeyCode inputType)
		{
			if (Input.GetKeyUp(inputType))
			{
				ExecuteInputData(m_InputKeyUpValues, inputType, InputType.GetKeyUp, 1);
			}
			else
			{
				ExecuteInputData(m_InputKeyUpValues, inputType, InputType.GetKeyUp, 0);
			}
		}

		private void ExecuteInputData(Dictionary<KeyCode, InputData> inputValueDic, KeyCode keyType, InputType inputType, int value_GetKey)
		{
			if (inputValueDic.TryGetValue(keyType, out InputData data))
			{
				data.SetDataValue(value_GetKey);
			}
			else
			{
				inputValueDic.Add(keyType, InputData.Create(keyType, inputType, value_GetKey));
			}
		}
	}
}
