﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace Cherry
{
	public class InputComponent : GameFrameworkComponent
	{
		private InputManager m_InputManager;

		protected override void Awake()
		{
			base.Awake();
		}

		// Start is called before the first frame update
		void Start()
		{
			m_InputManager = new InputManager();
		}

		// Update is called once per frame
		void Update()
		{
			m_InputManager.Update();
		}

		/// <summary>
		/// 设置键盘输入合法性。
		/// </summary>
		/// <param name="isValid">是否合法。</param>
		/// <param name="inputType">键盘输入类型。（按下，按住，松开）</param>
		/// <param name="keyCode">输入键类型。</param>
		public void SetInputKeyValid(bool isValid, InputType inputType, KeyCode keyCode)
		{
			m_InputManager.SetInputKeyValid(isValid, inputType, keyCode);
		}

		/// <summary>
		/// 键盘输入是否存在。
		/// </summary>
		/// <param name="inputType">键盘输入类型。（按下，按住，松开）</param>
		/// <param name="keyCode">输入键类型。</param>
		public bool IsInputKeyExist(InputType inputType, KeyCode keyCode)
		{
			return m_InputManager.IsInputKeyExist(inputType, keyCode);
		}

		public InputData GetInputKeyDataByKeyCode(InputType inputType, KeyCode keyCode)
		{
			return m_InputManager.GetInputKeyDataByKeyCode(inputType, keyCode);
		}
	}
}

