﻿using GameFramework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class InputData : IReference
	{
		public KeyCode KeyType
		{
			get;
			private set;
		}

		public InputType InputType
		{
			get;
			private set;
		}

		/// <summary>
		/// 上次按键的时间。
		/// </summary>
		public float LastTime
		{
			get;
			private set;
		}

		/// <summary>
		/// 当前按键的时间。
		/// </summary>
		public float Time
		{
			get;
			private set;
		}

		/// <summary>
		/// 按键值。
		/// </summary>
		public int Value
		{
			get;
		 	private set;
		}

		/// <summary>
		/// 按键响应是否合法。
		/// </summary>
		public bool IsValid
		{
			get;
			private set;
		}


		public static InputData Create(KeyCode keyType, InputType inputType, int value)
		{
			InputData data = ReferencePool.Acquire<InputData>();
			data.KeyType = keyType;
			data.InputType = inputType;
			data.Time = UnityEngine.Time.time;
			data.LastTime = 0;
			data.Value = value;
			data.IsValid = true;

			return data;
		}

		public void SetDataValue(int value)
		{
			if (value != 0)
			{
				LastTime = Time;
				Time = UnityEngine.Time.time;
			}

			Value = value;
		}

		public void SetDataValid(bool isValid)
		{
			IsValid = isValid;
		}

		public bool IsValidData()
		{
			return IsValid && Value == 1;
		}

		public void Clear()
		{
			KeyType = KeyCode.None;
			InputType = InputType.None;
			LastTime = 0;
			Time = 0;
			Value = 0;
		}
	}
}

