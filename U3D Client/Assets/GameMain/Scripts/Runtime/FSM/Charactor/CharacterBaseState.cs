﻿using GameFramework;
using GameFramework.Fsm;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	/// <summary>
	/// 角色状态抽象基类。
	/// </summary>
	public abstract class CharacterBaseState : FsmState<CharactorFSMManager>, IReference
	{
		protected override void OnInit(IFsm<CharactorFSMManager> fsm)
		{
			base.OnInit(fsm);
		}

		protected override void OnEnter(IFsm<CharactorFSMManager> fsm)
		{
			base.OnEnter(fsm);
		}

		protected override void OnLeave(IFsm<CharactorFSMManager> fsm, bool isShutdown)
		{
			base.OnLeave(fsm, isShutdown);
		}

		protected override void OnDestroy(IFsm<CharactorFSMManager> fsm)
		{
			base.OnDestroy(fsm);
		}

		protected override void OnUpdate(IFsm<CharactorFSMManager> fsm, float elapseSeconds, float realElapseSeconds)
		{
			//它的状态基类调用是先调用该基类的Update再调用子类的，因为调用子类的可能会进入其他状态，并走碰撞的Tick，碰撞的Tick必须先于该Update
			base.OnUpdate(fsm, elapseSeconds, realElapseSeconds);

			Charactor charactor = fsm.Owner.Charactor;
			float xOffset = charactor.VelocityX * Time.deltaTime;
			float yOffset = charactor.VelocityY * Time.deltaTime;
			if (charactor.ColliderType == TileColliderType.Ground)
			{
				yOffset = 0;
				charactor.VelocityY = 0;
			}
			charactor.transform.position += new Vector3(xOffset, yOffset, 0);
			if (!(Mathf.Abs(xOffset) < 0.01))
			{
				if (xOffset > 0)
				{
					charactor.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
				}
				else
				{
					charactor.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
				}
			}
		}

		public virtual void Clear()
		{
			GLogger.ErrorFormat(Log_Channel.FSM, "请设置改状态类的引用回收方法！State name：{0}", GetType().ToString());
		}
	}
}

