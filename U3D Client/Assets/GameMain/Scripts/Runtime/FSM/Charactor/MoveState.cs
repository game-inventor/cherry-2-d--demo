﻿using GameFramework.Fsm;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class MoveState : CharacterBaseState
	{
		protected override void OnInit(IFsm<CharactorFSMManager> fsm)
		{
			base.OnInit(fsm);
		}

		protected override void OnEnter(IFsm<CharactorFSMManager> fsm)
		{
			base.OnEnter(fsm);
			Charactor charactor = fsm.Owner.Charactor;
			charactor.AnimController.PlayAnimation(charactor.DRCharactorData.Anim_Move);
		}

		protected override void OnLeave(IFsm<CharactorFSMManager> fsm, bool isShutdown)
		{
			base.OnLeave(fsm, isShutdown);
		}

		protected override void OnDestroy(IFsm<CharactorFSMManager> fsm)
		{
			base.OnDestroy(fsm);
		}

		protected override void OnUpdate(IFsm<CharactorFSMManager> fsm, float elapseSeconds, float realElapseSeconds)
		{
			base.OnUpdate(fsm, elapseSeconds, realElapseSeconds);

			if (fsm.Owner.Charactor is Player)
			{
				Player player = fsm.Owner.Charactor as Player;
				bool isMove = false;
				bool isJump = false;

				InputData leftArrowData = GameEntry.Input.GetInputKeyDataByKeyCode(InputType.GetKey, KeyCode.LeftArrow);
				InputData rightArrowData = GameEntry.Input.GetInputKeyDataByKeyCode(InputType.GetKey, KeyCode.RightArrow);
				InputData spaceArrowData = GameEntry.Input.GetInputKeyDataByKeyCode(InputType.GetKeyDown, KeyCode.Space);
				if (leftArrowData.IsValidData() && rightArrowData.IsValidData() || !leftArrowData.IsValidData() && !rightArrowData.IsValidData())
				{
					float offsetX = Mathf.Abs(player.VelocityX) - player.SpeedX;
					if (offsetX <= 0)
					{
						player.VelocityX = 0;
					}
					else
					{
						player.VelocityX = player.VelocityX > 0 ? offsetX : -offsetX;
					}
				}
				else
				{
					//判断左移还是右移
					player.VelocityX += rightArrowData.Value > leftArrowData.Value ? player.SpeedX : -player.SpeedX;
					isMove = true;
				}

				if (spaceArrowData.Value == 1)
				{
					isJump = true;
				}

				if (isJump)
				{
					player.VelocityY = 5;
					ChangeState<FloatingState>(fsm);
				}
				else
				{
					if (Mathf.Abs(player.VelocityX) < 0.01 && !isMove)
					{
						player.VelocityX = 0;
						ChangeState<IdleState>(fsm);
					}
				}
			}
		}
	}
}

