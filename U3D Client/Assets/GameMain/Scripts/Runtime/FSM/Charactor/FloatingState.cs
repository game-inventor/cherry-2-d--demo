﻿using GameFramework.Fsm;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class FloatingState : CharacterBaseState
	{
		protected override void OnInit(IFsm<CharactorFSMManager> fsm)
		{
			base.OnInit(fsm);
		}

		protected override void OnEnter(IFsm<CharactorFSMManager> fsm)
		{
			base.OnEnter(fsm);
			Charactor charactor = fsm.Owner.Charactor;

			//因为跳跃或者降落需要判定碰撞，所以直接给一个位移
			if (charactor.VelocityY > 0)
			{
				charactor.AnimController.PlayAnimation(charactor.DRCharactorData.Anim_Jump);
				charactor.transform.position += new Vector3(0, 0.1f, 0);
			}
			else
			{
				charactor.AnimController.PlayAnimation(charactor.DRCharactorData.Anim_Fall);
				charactor.transform.position += new Vector3(0, -0.1f, 0);
			}
		}

		protected override void OnLeave(IFsm<CharactorFSMManager> fsm, bool isShutdown)
		{
			base.OnLeave(fsm, isShutdown);
		}

		protected override void OnDestroy(IFsm<CharactorFSMManager> fsm)
		{
			base.OnDestroy(fsm);
		}

		protected override void OnUpdate(IFsm<CharactorFSMManager> fsm, float elapseSeconds, float realElapseSeconds)
		{
			base.OnUpdate(fsm, elapseSeconds, realElapseSeconds);

			Charactor charactor = fsm.Owner.Charactor;
			if (charactor.ColliderType == TileColliderType.None)
			{
				charactor.VelocityY -= charactor.Gravity;
				//ChangeState<FloatingState>(fsm);
			}

			if (charactor.VelocityY > 0)
			{
				if (charactor.AnimController.IsPlayingAnimation(charactor.DRCharactorData.Anim_Fall))
				{
					charactor.AnimController.PlayAnimation(charactor.DRCharactorData.Anim_Jump);
				}
			}
			else
			{
				if (charactor.AnimController.IsPlayingAnimation(charactor.DRCharactorData.Anim_Jump))
				{
					charactor.AnimController.PlayAnimation(charactor.DRCharactorData.Anim_Fall);
				}
			}

			if (charactor.ColliderType == TileColliderType.Ground && charactor.VelocityY == 0)
			{
				if (charactor.VelocityX != 0)
				{
					ChangeState<MoveState>(fsm);
				}
				else
				{
					ChangeState<IdleState>(fsm);
				}

				if (Mathf.Abs(charactor.VelocityY) <= charactor.Gravity)
				{
					charactor.VelocityY = 0;
				}
			}
		}
	}
}

