﻿using GameFramework.Fsm;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace Cherry
{
	/// <summary>
	/// 角色状态机管理器。
	/// </summary>
	/// <remarks>角色状态机持有者，Player和NPC持有该状态机管理器。 </remarks>
	public class CharactorFSMManager
	{
		private IFsm<CharactorFSMManager> m_Fsm;
		private Charactor m_Charactor;


		private bool m_IsSetCharactor = false;
		public Charactor Charactor
		{
			get { return m_Charactor; }
			set
			{
				if (m_IsSetCharactor)
				{
					GLogger.ErrorFormat(Log_Channel.FSM, "不能重复设置状态机的角色！Charactor gameObject name：{0}", value.name);
					return;
				}

				m_Charactor = value;
				m_IsSetCharactor = true;
			}
		}




		public CharactorFSMManager()
		{
			m_Fsm = null;
		}

		public void CreateFsm(params CharacterBaseState[] states)
		{
			m_Fsm = GameEntry.Fsm.CreateFsm(this, states);
		}

		public void Start<T>() where T : CharacterBaseState
		{
			if (m_Fsm == null)
			{
				GLogger.Fatal(Log_Channel.FSM, "开启有限状态机逻辑前必须创建该有限状态机！");
				return;
			}

			m_Fsm.Start<T>();
		}

		public void Start(Type type)
		{
			if (m_Fsm == null)
			{
				GLogger.Fatal(Log_Channel.FSM, "开启有限状态机逻辑前必须创建该有限状态机！");
				return;
			}

			m_Fsm.Start(type);
		}

		public void GetState<T>() where T : CharacterBaseState
		{
			if (m_Fsm == null)
			{
				GLogger.Fatal(Log_Channel.FSM, "获取有限状态机状态前必须创建该有限状态机！");
				return;
			}

			m_Fsm.GetState<T>();
		}

		public void GetState(Type type)
		{
			if (m_Fsm == null)
			{
				GLogger.Fatal(Log_Channel.FSM, "获取有限状态机状态前必须创建该有限状态机！");
				return;
			}

			m_Fsm.GetState(type);
		}

		public bool HasState<T>() where T : CharacterBaseState
		{
			if (m_Fsm == null)
			{
				GLogger.Fatal(Log_Channel.FSM, "判断是否有状态前必须创建该有限状态机！");
				return false;
			}

			return m_Fsm.HasState<T>();
		}

		public bool HasState(Type type)
		{
			if (m_Fsm == null)
			{
				GLogger.Fatal(Log_Channel.FSM, "判断是否有状态前必须创建该有限状态机！");
				return false;
			}

			return m_Fsm.HasState(type);
		}
	}
}

