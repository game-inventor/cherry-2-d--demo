﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
using System;
using System.IO;

#if UNITY_EDITOR
namespace Cherry.Tilemaps
{
	public static class TilemapHelper
	{
		private static string TilemapSourceDataPath = "Assets/GameMain/Scripts/Editor/Chunk/SourceTilemapData.asset";
		private static string TilemapTargetDataPath = "Assets/GameMain/Scripts/Editor/Chunk/TargetTilemapData.asset";

		private static TilemapData m_SourceData;
		public static TilemapData SourceMapData
		{
			get
			{
				if (m_SourceData == null)
				{
					m_SourceData = AssetDatabase.LoadAssetAtPath<TilemapData>(TilemapSourceDataPath);
				}

				return m_SourceData;
			}
		}

		private static TilemapData m_TargetData;
		public static TilemapData TargetMapData
		{
			get
			{
				if (m_TargetData == null)
				{
					m_TargetData = AssetDatabase.LoadAssetAtPath<TilemapData>(TilemapTargetDataPath);
				}

				return m_TargetData;
			}

			set
			{
				m_TargetData = value;
			}
		}

		/// <summary>
		/// 添加地图数据。
		/// </summary>
		/// <param name="brushTarget">笔刷</param>
		/// <param name="pos">世界坐标</param>
		/// <param name="data">单位数据</param>
		public static void AddData(GameObject brushTarget, Vector3 pos, TileInfo data)
		{
			Tilemap tilemap = brushTarget.GetComponent<Tilemap>();
			
			if (tilemap.name != GetTilemapFullName(TargetMapData))
			{
				return;
			}

			bool isadd = true;
			for (int i = 0; i < TargetMapData.TileInfoList.Count; i++)
			{
				//如果在相同位置，则只修改TileInfo
				if (TargetMapData.TileInfoList[i].Pos == pos)
				{
					isadd = false;
					TargetMapData.TileInfoList[i] = data;
					break;
				}
			}

			if (isadd)
			{
				TargetMapData.TileInfoList.Add(data);
			}
		}

		/// <summary>
		/// 清除地图数据。
		/// </summary>
		/// <param name="brushTarget">笔刷</param>
		/// <param name="pos">世界坐标</param>
		public static void ClearData(GameObject brushTarget, Vector3 pos)
		{
			Tilemap tilemap = brushTarget.GetComponent<Tilemap>();
			if (tilemap.name != GetTilemapFullName(TargetMapData))
			{
				return;
			}

			for (int i = 0; i < TargetMapData.TileInfoList.Count; i++)
			{
				if (TargetMapData.TileInfoList[i].Pos == pos)
				{
					TargetMapData.TileInfoList.RemoveAt(i);
				}
			}
		}

		public static void SaveTilemapData(string path)
		{
			TilemapData data = AssetDatabase.LoadAssetAtPath<TilemapData>(path);
			data = CloneMapData(TargetMapData);
			Debug.Log("保存TilemapData文件成功，文件路径为：" + path);
		}

		public static void CreateTilemapData(string path)
		{
			TilemapData data = CloneMapData(TargetMapData);

			try
			{
				AssetDatabase.CreateAsset(data, path + ".asset");
			}
			catch (Exception e)
			{
				throw new Exception(string.Format("创建TilemapData文件失败，路径为{0}，message：{1}", path, e));
			}

			Debug.Log("创建TilemapData文件成功，文件路径为：" + path);
		}

		public static void SetTargetData(TilemapData data)
		{
			if (data == null)
			{
				GUILayout.Box("加载TilemapData文件失败");
				return;
			}
			TargetMapData = CloneMapData(data);

			Debug.Log("读取TilemapData文件成功");
		}
		
		public static TilemapData CloneMapData(TilemapData data)
		{
			if (data == null)
			{
				Debug.LogError("克隆的TilemapData为null！");
				return null;
			}

			TilemapData cloneData = ScriptableObject.CreateInstance<TilemapData>();

			cloneData.TilemapGroupName = data.TilemapGroupName;
			cloneData.TilemapName = data.TilemapName;
			cloneData.OrderInLayer = data.OrderInLayer;
			cloneData.SortOrderIndex = data.SortOrderIndex;
			cloneData.ChunkBeginPosX = data.ChunkBeginPosX;
			cloneData.ChunkBeginPosY = data.ChunkBeginPosY;
			cloneData.ChunkHeight = data.ChunkHeight;
			cloneData.ChunkWidth = data.ChunkWidth;
			cloneData.ScriptName = data.ScriptName;
			cloneData.TileInfoList.Clear();
			foreach (var item in data.TileInfoList)
			{
				cloneData.TileInfoList.Add(item);
			}

			return cloneData;
		}

		public static string GetTilemapFullName(TilemapData data)
		{
			return string.Format("{0} - {1}", data.TilemapName, data.TilemapGroupName);
		}

		/// <summary>
		/// 地图ID
		/// </summary>
		public static int MapID = 0;

		public static Vector2 tileOffset2 = new Vector2(0.5f, 0.5f);
		public static Vector3 tileOffset3 = new Vector3(0.5f, 0.5f, 0);

#if UNITY_EDITOR
		/// <summary>
		/// 载入地图数据
		/// </summary>
		/// <param name="path"></param>
		private static void LoadData(string path)
		{

		}

		/// <summary>
		/// 设置Tile
		/// </summary>
		/// <param name="map"></param>
		/// <param name="pos"></param>
		/// <param name="tilebase"></param>
		public static void SetTile(Tilemap map, Vector3Int pos, TileBase tilebase)
		{
			map.SetTile(pos, tilebase);
		}

		/// <summary>
		/// 设置TileMap
		/// </summary>
		/// <param name="map"></param>
		/// <param name="tileMapDataList"></param>
		public static void SetTileMap(Tilemap map, TilemapData tileMapData)
		{
			if (tileMapData.TileInfoList != null)
			{
				foreach (var tile in tileMapData.TileInfoList)
				{
					map.SetTile(tile.IntPos, tile.Tile);
				}
			}
		}

		///// <summary>
		///// 初始化地图,绑定寻路数据
		///// </summary>
		//public static void InitMap()
		//{
		//	//Debug.Log(XMMapData.mapSize);
		//	TilemapManager = new Dictionary<Vector2, Point>();
		//	List<TileMapData> tilemapData = TilemapManager.MapData.Data[TilemapManager.MapID].tileMapDataList;
		//	foreach (var item in tilemapData)
		//	{
		//		for (int i = 0; i < item.tileInfoList.Count; i++)
		//		{
		//			int x = item.tileInfoList[i].ipos.x;
		//			int y = item.tileInfoList[i].ipos.y;
		//			//Debug.Log(x + " " + y);
		//			XMMapData.map.Add(new Vector2(x, y), new Point(x, y));
		//			bool walkable = ((XMTile)item.tileInfoList[i].tile).walkable;
		//			if (walkable)
		//			{
		//				XMMapData.map[new Vector2(x, y)].Walkable = walkable;
		//			}
		//		}
		//	}
		//}




		public static void ClearDataForPos(Vector3 pos)
		{

		}

		/// <summary>
		/// 清空数据
		/// </summary>
		public static void ClearAllData()
		{

		}
#endif
	}
}
#endif