﻿using GameFramework.Event;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkLoadedEventArgs : GameEventArgs
{
	public static int EventId = typeof(ChunkLoadedEventArgs).GetHashCode();
	public override int Id
	{
		get
		{
			return EventId;
		}
	}

	public override void Clear()
	{
		EventId = 0;
	}
}
