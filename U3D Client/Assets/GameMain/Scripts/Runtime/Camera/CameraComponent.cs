﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace Cherry
{
	public class CameraComponent : GameFrameworkComponent
	{
		private CameraManager m_CameraManager;

		protected override void Awake()
		{
			base.Awake();
		}

		private void Start()
		{
			m_CameraManager = new CameraManager();
			Camera mainCamera = transform.Find("Main Camera").GetComponent<Camera>();
			m_CameraManager.Init(mainCamera);
		}

		private void Update()
		{
			m_CameraManager.Update();
		}

		public void SetCameraPlayer(Player player)
		{
			m_CameraManager.SetPlayer(player);
		}
	}
}

