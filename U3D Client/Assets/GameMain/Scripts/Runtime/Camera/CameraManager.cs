﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class CameraManager
	{
		private Camera m_MainCamera;
		private Player m_Player;

		public void Init(Camera mainCamera)
		{
			m_MainCamera = mainCamera;
		}

		public void Update()
		{
			//Main Camera 实时更新位置到玩家位置
			if (m_Player != null)
			{
				Vector3 playerPosition = m_Player.transform.position;
				m_MainCamera.transform.position = new Vector3(playerPosition.x,playerPosition.y, m_MainCamera.transform.position.z);
			}
		}

		public void SetPlayer(Player player)
		{
			m_Player = player;
		}
	}
}
