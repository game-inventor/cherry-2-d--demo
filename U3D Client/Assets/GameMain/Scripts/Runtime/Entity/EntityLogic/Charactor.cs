﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class Charactor : BaseEntityLogic
	{
		protected CharactorFSMManager m_FsmManager;
		protected SampleAnimController m_AnimController;
		protected DRCharactor m_DRCharactorData;
		protected TileColliderType m_ColliderType = TileColliderType.None;

		private float m_SpeedX = 1;
		private float m_Gravity = 0.2f;
		[SerializeField]
		private float m_VelocityX;
		private float m_MaxVelocityX = 5;
		[SerializeField]
		private float m_VelocityY;
		private float m_MaxVelocityY = 5;

		/// <summary>
		/// 角色动画控制器。
		/// </summary>
		public SampleAnimController AnimController
		{
			get { return m_AnimController; }
			set { m_AnimController = value; }
		}

		/// <summary>
		/// 角色表数据。
		/// </summary>
		public DRCharactor DRCharactorData
		{
			get { return m_DRCharactorData; }
			set { m_DRCharactorData = value; }
		}

		/// <summary>
		/// 角色Tile碰撞类型。
		/// </summary>
		public TileColliderType ColliderType
		{
			get { return m_ColliderType; }
			set { m_ColliderType = value; }
		}

		/// <summary>
		/// 角色移动X轴加速度。
		/// </summary>
		public float SpeedX
		{
			get { return m_SpeedX; }
			set { m_SpeedX = value; }
		}

		/// <summary>
		/// 角色重力加速度。
		/// </summary>
		public float Gravity
		{
			get { return m_Gravity; }
			set { m_Gravity = value; }
		}

		/// <summary>
		/// 角色当前X轴速度。
		/// </summary>
		public float VelocityX
		{
			get { return m_VelocityX; }
			set 
			{
				if (m_MaxVelocityX < Mathf.Abs(value))
				{
					if (value > 0)
						m_VelocityX = m_MaxVelocityX;
					else
						m_VelocityX = -m_MaxVelocityX;
					return;
				}

				m_VelocityX = value; 
			}
		}

		/// <summary>
		/// 角色当前Y轴速度。
		/// </summary>
		public float VelocityY
		{
			get { return m_VelocityY; }
			set 
			{
				if (m_MaxVelocityY < Mathf.Abs(value))
				{
					if(value > 0)
						m_VelocityY = m_MaxVelocityY;
					else
						m_VelocityY = -m_MaxVelocityY;
					return;
				}

				m_VelocityY = value; 
			}
		}

		#region 实体生命周期函数
		protected override void OnInit(object userData)
		{
			base.OnInit(userData);

			EntityParams entityParams = userData as EntityParams;
			CharactorData data = entityParams.EntityData as CharactorData;
			m_DRCharactorData = data.DRCharactorData;

			//初始化角色状态机
			m_FsmManager = new CharactorFSMManager();
			m_FsmManager.CreateFsm(
				ReferencePool.Acquire<IdleState>(),
				ReferencePool.Acquire<MoveState>(),
				ReferencePool.Acquire<FloatingState>());

			//初始化角色动画管理器
			Animator animator = GetComponent<Animator>();
			AnimationPreloader preloader = GetComponent<AnimationPreloader>();
			m_AnimController = new SampleAnimController(animator);
			m_AnimController.Init(preloader.AnimationClips, name);
		}

		protected override void OnRecycle()
		{
			base.OnRecycle();
			m_AnimController.Destroy();
		}

		protected override void OnShow(object userData)
		{
			base.OnShow(userData);
		}

		protected override void OnHide(bool isShutdown, object userData)
		{
			base.OnHide(isShutdown, userData);
		}

		protected override void OnAttached(BaseEntityLogic childEntity, Transform parentTransform, object userData)
		{
			base.OnAttached(childEntity, parentTransform, userData);
		}

		protected override void OnDetached(BaseEntityLogic childEntity, object userData)
		{
			base.OnDetached(childEntity, userData);
		}

		protected override void OnAttachTo(BaseEntityLogic parentEntity, Transform parentTransform, object userData)
		{
			OnAttached(parentEntity, parentTransform, userData);
		}

		protected override void OnDetachFrom(BaseEntityLogic parentEntity, object userData)
		{
			base.OnDetachFrom(parentEntity, userData);
		}

		protected override void OnUpdate(float elapseSeconds, float realElapseSeconds)
		{
			base.OnUpdate(elapseSeconds, realElapseSeconds);
		}

		protected override void InternalSetVisible(bool visible)
		{
			base.InternalSetVisible(visible);
		}
		#endregion
	}
}

