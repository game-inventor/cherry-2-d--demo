﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class Player : Charactor
	{

		#region 实体生命周期函数
		protected override void OnInit(object userData)
		{
			base.OnInit(userData);

			//等基类Charactor创建完后设置状态机属性
			m_FsmManager.Charactor = this;

			//相机
			GameEntry.Camera.SetCameraPlayer(this);
		}

		protected override void OnRecycle()
		{
			base.OnRecycle();
		}

		protected override void OnShow(object userData)
		{
			base.OnShow(userData);
			m_FsmManager.Start<FloatingState>();
		}

		protected override void OnHide(bool isShutdown, object userData)
		{
			base.OnHide(isShutdown, userData);
		}

		protected override void OnAttached(BaseEntityLogic childEntity, Transform parentTransform, object userData)
		{
			base.OnAttached(childEntity, parentTransform, userData);
		}

		protected override void OnDetached(BaseEntityLogic childEntity, object userData)
		{
			base.OnDetached(childEntity, userData);
		}

		protected override void OnAttachTo(BaseEntityLogic parentEntity, Transform parentTransform, object userData)
		{
			OnAttached(parentEntity, parentTransform, userData);
		}

		protected override void OnDetachFrom(BaseEntityLogic parentEntity, object userData)
		{
			base.OnDetachFrom(parentEntity, userData);
		}

		protected override void OnUpdate(float elapseSeconds, float realElapseSeconds)
		{
			base.OnUpdate(elapseSeconds, realElapseSeconds);
		}

		protected override void InternalSetVisible(bool visible)
		{
			base.InternalSetVisible(visible);
		}
		#endregion

		private void OnCollisionEnter2D(Collision2D collision)
		{
			Debug.LogWarning("进入！");
			//暂时简单的都设置为Ground，后续根据地图和碰撞位置数据找到对应的类型。
			m_ColliderType = TileColliderType.Ground;
			Debug.LogWarning(collision.contacts[0].point);
		}

		private void OnCollisionStay2D(Collision2D collision)
		{

		}

		private void OnCollisionExit2D(Collision2D collision)
		{
			Debug.LogWarning("离开！");
			m_ColliderType = TileColliderType.None;
		}
	}
}

