﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace Cherry
{
	/// <summary>
	/// 实体组件扩展类。
	/// </summary>
	public static class EntityExtension
	{
		private static int m_SerialEntityId = 0;

		public static void ShowEntity<T>(this EntityComponent component, string entityAssetName, string entityGroupName, BaseEntityData entityData, object userdata = null) where T : EntityLogic
		{
			int entityId = GetSerialEntityId();
			EntityParams entityParams = EntityParams.Create(entityData, userdata);
			component.ShowEntity<T>(entityId,entityAssetName,entityGroupName,entityParams);
		}

		private static int GetSerialEntityId()
		{
			return ++m_SerialEntityId;
		}
	}
}

