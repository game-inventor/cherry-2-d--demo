﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class EntityParams : IReference
	{
		public BaseEntityData EntityData
		{
			get;
			private set;
		}

		public object Userdata
		{
			get;
			private set;
		}

		public EntityParams()
		{
			
		}

		public static EntityParams Create(BaseEntityData entityData, object userdata)
		{
			EntityParams entityParams = new EntityParams();
			entityParams.EntityData = entityData;
			entityParams.Userdata = userdata;
			return entityParams;
		}

		public void Clear()
		{
			EntityData = null;
			Userdata = null;
		}
	}
}

