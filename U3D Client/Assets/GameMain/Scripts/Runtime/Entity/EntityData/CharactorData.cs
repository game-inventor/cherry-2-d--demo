﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class CharactorData : BaseEntityData
	{
		public DRCharactor DRCharactorData;

		public override void Clear()
		{
			DRCharactorData = null;
			base.Clear();
		}
	}
}

