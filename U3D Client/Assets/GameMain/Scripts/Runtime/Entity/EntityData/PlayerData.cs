﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cherry
{
	public class PlayerData : CharactorData
	{
		public static PlayerData Create(int charactorTid,Vector3 position, Quaternion rotation)
		{
			PlayerData data = ReferencePool.Acquire<PlayerData>();
			data.Position = position;
			data.Rotation = rotation;
			data.DRCharactorData = GameEntry.DataTable.GetDataTable<DRCharactor>().GetDataRow(charactorTid);
			return data;
		}
	}
}

