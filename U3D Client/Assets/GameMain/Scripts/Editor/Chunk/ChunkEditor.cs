﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using Cherry.Tilemaps;
using UnityEngine.Tilemaps;
using System.IO;
using System.Text;
using System.Reflection;
using System;

namespace Cherry.Editor.Tilemaps
{
	/// <summary>
	/// 地图编辑器。
	/// </summary>
	public class ChunkEditor : EditorWindow
	{
		private readonly string MapFolderPath = "Assets/GameMain/AssetData/Map";

		public int toolbarOption = 0;
		public string[] toolbarTexts = { "编 辑", "设 置", "关 于" };

		//文件相关
		DirectoryInfo[] directoryInfos;
		private int directoryToolbarIndex = 0;
		private int lastDirectoryToolbarIndex = 0;//用于判断文件夹目录是否更改
		int[] directoryIntPopupSize;
		private string[] directoryToolbarTexts;

		private int fileToolbarIndex = 0;
		int[] fileIntPopupSize;
		private string[] fileToolbarTexts;

		//画线相关
		public static bool IsDrawLine = true;
		public static string CurEditMapName = "";//map name：groupName + " - " + mapName
		public static Dictionary<string, TilemapData> LoadedMapDatas;
		private TilemapData m_LastEditData;

		//搜索栏
		private string searchText;
		private int searchCount;
		private StringBuilder m_StringBuilder;

		int selectedIndex = 0;

		Vector2 scrollPos;
		Vector3 delVecPos;


		#region ShowTileMapEditor
		[MenuItem("CherryTool/地图编辑器")]
		public static void ShowTilemapEditor()
		{
			if (EditorSceneManager.GetActiveScene().name == "TilemapEditScene")
			{
				ChunkEditor window = (ChunkEditor)GetWindow(typeof(ChunkEditor));
				window.titleContent = new GUIContent("地图编辑器");
				window.Show();

				Type type = Assembly.Load("Unity.2D.Tilemap.Editor").GetType("UnityEditor.Tilemaps.GridPaintPaletteWindow");
				EditorWindow tileWindow = (EditorWindow)ScriptableObject.CreateInstance(type);
				tileWindow.titleContent = new GUIContent("Tile Palette");
				type.GetMethod("Show", Type.EmptyTypes).Invoke(tileWindow, null);
			}
			else
			{
				if (EditorUtility.DisplayDialog("提示","打开编辑器需要跳转到TilemapEditScene场景，点击确认将会自动保存当前场景，点击取消则继续提留在当前场景","确定残忍离开~","取消离开选项~"))
				{
					EditorSceneManager.SaveOpenScenes();
					EditorSceneManager.OpenScene("Assets/GameMain/Scenes/TilemapEditScene.unity");

					ChunkEditor window = (ChunkEditor)GetWindow(typeof(ChunkEditor));
					window.titleContent = new GUIContent("地图编辑器");
					window.Show();

					Type type = Assembly.Load("Unity.2D.Tilemap.Editor").GetType("UnityEditor.Tilemaps.GridPaintPaletteWindow");
					EditorWindow tileWindow = (EditorWindow)ScriptableObject.CreateInstance(type);
					tileWindow.titleContent = new GUIContent("Tile Palette");
					type.GetMethod("Show", Type.EmptyTypes).Invoke(tileWindow, null);
				}
			}
		}

		private void OnEnable()
		{
			m_StringBuilder = new StringBuilder(1024);
			LoadedMapDatas = new Dictionary<string, TilemapData>();
			m_LastEditData = null;
			CurEditMapName = "";
			Debug.Log("OnEnable");
		}

		private void OnDestroy()
		{
			foreach (var data in LoadedMapDatas)
			{
				GameObject go = GameObject.Find(data.Key);

				if (go != null)
				{
					DestroyImmediate(go);
				}
			}

			GameObject grid = GameObject.Find("Grid");
			if (grid != null)
			{
				DestroyImmediate(grid);
			}

			CurEditMapName = "";
			LoadedMapDatas.Clear();
		}
		#endregion
		
		#region OnGUI
		private void OnGUI()
		{
			EditorGUILayout.Space();
			MainGUI();
			GUILayout.Space(5);
			GUI.backgroundColor = Color.gray;
			toolbarOption = GUILayout.Toolbar(toolbarOption, toolbarTexts, GUILayout.Height(30));
			switch (toolbarOption)
			{
				case 0:
					Title("Editor");
					TilemapContent();
					break;
				case 1:
					Title("Setting");
					SettingContent();
					break;
				case 2:
					Title("About");
					AboutContent();
					break;
				default:
					break;
			}
		}

		private void Title(string operationName)
		{
			GUILayout.Label(operationName, EditorStyles.boldLabel);
			EditorGUILayout.Space();
			GUI.backgroundColor = Color.white;
		}
		#endregion

		#region MainGUI
		private void MainGUI()
		{
			GUILayout.BeginVertical();
			DirectoryInfo directoryInfo = new DirectoryInfo(MapFolderPath);
			directoryInfos = directoryInfo.GetDirectories();
			//地图文件夹目录
			EditorGUIUtility.labelWidth = 70;
			directoryToolbarTexts = new string[directoryInfos.Length];
			directoryIntPopupSize = new int[directoryInfos.Length];
			for (int i = 0; i < directoryInfos.Length; i++)
			{
				directoryIntPopupSize[i] = i;
				directoryToolbarTexts[i] = directoryInfos[i].Name;
			}
			directoryToolbarIndex = EditorGUILayout.IntPopup("地图组目录：", directoryToolbarIndex, directoryToolbarTexts, directoryIntPopupSize, GUILayout.Width(directoryToolbarTexts[directoryToolbarIndex].Length * 20)/*,GUILayout.ExpandWidth(true)*/);
			if (directoryToolbarIndex != lastDirectoryToolbarIndex)
			{
				lastDirectoryToolbarIndex = directoryToolbarIndex;
				fileToolbarIndex = 0;
			}

			//地图文件目录
			FileInfo[] fileInfos = directoryInfos[directoryToolbarIndex].GetFiles("*.asset");
			fileToolbarTexts = new string[fileInfos.Length];
			for (int i = 0; i < fileInfos.Length; i++)
			{
				fileToolbarTexts[i] = fileInfos[i].Name;
			}
			if (fileToolbarTexts.Length == 0)
			{
				GUILayout.Label("副本为空，请设置副本数据");
				return;
			}
			fileIntPopupSize = new int[fileToolbarTexts.Length];
			for (int i = 0; i < fileToolbarTexts.Length; i++)
			{
				fileIntPopupSize[i] = i;
			}

			fileToolbarIndex = EditorGUILayout.IntPopup("地图块目录：", fileToolbarIndex, fileToolbarTexts, fileIntPopupSize, GUILayout.Width(fileToolbarTexts[fileToolbarIndex].Length * 20));
			GUILayout.EndVertical();

			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("打开编辑场景", GUILayout.Height(35)))
			{
				if (EditorSceneManager.GetActiveScene().name == "TilemapEditScene")
				{
					EditorUtility.DisplayDialog("提示", "已经在地图编辑场景。", "确认");
					return;
				}
				else
				{
					EditorSceneManager.OpenScene("Assets/GameMain/Scenes/TilemapEditScene.unity");
				}
			}
			if (GUILayout.Button("预览当前地图块目录地图（非编辑）", GUILayout.Height(35)))
			{
				TilemapData data = AssetDatabase.LoadAssetAtPath<TilemapData>(MapFolderPath + "/" + directoryToolbarTexts[directoryToolbarIndex] + "/" + fileToolbarTexts[fileToolbarIndex]);
				CreatePreviewMap(data);
			}

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("保存当前地图数据到当前文件目录", GUILayout.Height(35)))
			{
				SaveOrCreateData(MapFolderPath + "/" +directoryToolbarTexts[directoryToolbarIndex]);
			}
			if (GUILayout.Button("读取当前文件目录的地图数据并编辑该地图", GUILayout.Height(35)))
			{
				if (CurEditMapName == "")
				{
					TilemapData data = AssetDatabase.LoadAssetAtPath<TilemapData>(MapFolderPath + "/" + directoryToolbarTexts[directoryToolbarIndex] + "/" + fileToolbarTexts[fileToolbarIndex]);
					CreateCurTileMap(data);
				}
				else
				{
					TilemapData data = AssetDatabase.LoadAssetAtPath<TilemapData>(MapFolderPath + "/" + directoryToolbarTexts[directoryToolbarIndex] + "/" + fileToolbarTexts[fileToolbarIndex]);
					string name = GetTilemapFullName(data);
					if (CurEditMapName == name)
					{
						EditorUtility.DisplayDialog("提示", string.Format("正在编辑{0}地图。", name), "确认");
						Selection.activeObject = GameObject.Find(name);
					}
					else
					{
						if (EditorUtility.DisplayDialog("读取当前文件目录的地图数据并编辑该地图", string.Format("上次编辑的地图名为{0}请确认是否保存！当前要读取并编辑的地图文件地图组名为【{1}】，地图名为【{2}】",
							CurEditMapName,
							directoryToolbarTexts[directoryToolbarIndex],
							fileToolbarTexts[fileToolbarIndex]), "确定", "取消"))
						{
							CreateCurTileMap(data);
						}
					}
				}
			}
			EditorGUILayout.EndHorizontal();

			Repaint();
		}

		private void SaveOrCreateData(string path)
		{
			path = path + "/" + TilemapHelper.TargetMapData.TilemapName;
			if (File.Exists(path))
			{
				if (EditorUtility.DisplayDialog("保存地图数据文件", string.Format("当前地图数据文件将会保存到路径【{0}】，文件名为【{1}】", directoryInfos[directoryToolbarIndex], TilemapHelper.TargetMapData.TilemapName), "确定", "取消"))
				{
					TilemapHelper.SaveTilemapData(path);
				}
			}
			else
			{
				if (EditorUtility.DisplayDialog("创建新的地图数据文件", string.Format("正在创建新的地图数据文件！路径【{0}】，文件名为【{1}】", directoryInfos[directoryToolbarIndex], TilemapHelper.TargetMapData.TilemapName), "确定", "取消"))
				{
					TilemapHelper.CreateTilemapData(path);
				}
			}
		}

		#endregion

		#region TilemapContent
		private void TilemapContent()
		{
			EditorGUIUtility.labelWidth = 120;
			GUILayout.BeginHorizontal();
			MapDataLeft();
			MapDataRight();
			GUILayout.EndHorizontal();
			SearchTileContent();
		}

		private void MapDataLeft()
		{
			GUILayout.BeginVertical();
			string[] StrlayersPopupSize = new string[SortingLayer.layers.Length];
			int[] intlayersPopupSize = new int[SortingLayer.layers.Length];
			for (int i = 0; i < intlayersPopupSize.Length; i++)
			{
				intlayersPopupSize[i] = i;
				StrlayersPopupSize[i] = SortingLayer.layers[i].name;
			}
			string[] SortOrderPopupSize = { TilemapRenderer.SortOrder.BottomLeft.ToString(), TilemapRenderer.SortOrder.BottomRight.ToString(), TilemapRenderer.SortOrder.TopLeft.ToString(), TilemapRenderer.SortOrder.TopRight.ToString() };
			int[] intSortOrderPopupSize = { (int)TilemapRenderer.SortOrder.BottomLeft, (int)TilemapRenderer.SortOrder.BottomRight, (int)TilemapRenderer.SortOrder.TopLeft, (int)TilemapRenderer.SortOrder.TopRight };

			TilemapHelper.TargetMapData.TilemapGroupName = EditorGUILayout.TextField("地图组名称 : ", TilemapHelper.TargetMapData.TilemapGroupName, GUILayout.Width(250));
			TilemapHelper.TargetMapData.TilemapName = EditorGUILayout.TextField("地图名称 : ", TilemapHelper.TargetMapData.TilemapName, GUILayout.Width(250));
			TilemapHelper.TargetMapData.SortOrderIndex = EditorGUILayout.IntPopup("Sort Order：", TilemapHelper.TargetMapData.SortOrderIndex, SortOrderPopupSize, intSortOrderPopupSize, GUILayout.ExpandWidth(true), GUILayout.Width(250));
			TilemapHelper.TargetMapData.SortingLayerIndex = EditorGUILayout.IntPopup("Sorting Layer：", TilemapHelper.TargetMapData.SortingLayerIndex, StrlayersPopupSize, intlayersPopupSize, GUILayout.ExpandWidth(true), GUILayout.Width(250));
			TilemapHelper.TargetMapData.OrderInLayer = EditorGUILayout.IntField("Order in Layer : ", TilemapHelper.TargetMapData.OrderInLayer, GUILayout.Width(250));
			TilemapHelper.TargetMapData.ChunkWidth = EditorGUILayout.IntField("地图X轴长度 : ", TilemapHelper.TargetMapData.ChunkWidth, GUILayout.Width(250));
			TilemapHelper.TargetMapData.ChunkHeight = EditorGUILayout.IntField("地图Y轴长度 : ", TilemapHelper.TargetMapData.ChunkHeight, GUILayout.Width(250));
			TilemapHelper.TargetMapData.ChunkBeginPosX = EditorGUILayout.IntField("起始Tile位置X坐标 : ", TilemapHelper.TargetMapData.ChunkBeginPosX, GUILayout.Width(250));
			TilemapHelper.TargetMapData.ChunkBeginPosY = EditorGUILayout.IntField("起始Tile位置Y坐标 : ", TilemapHelper.TargetMapData.ChunkBeginPosY, GUILayout.Width(250));

			//获得所有ChunkLogic脚本名，去除基类
			List<string> typeNames = new List<string>();
			Type[] types = Assembly.Load("Assembly-CSharp").GetTypes();
			foreach (Type type in types)
			{
				if (type.IsClass && !type.IsAbstract && typeof(ChunkLogic).IsAssignableFrom(type) && type.Name != typeof(ChunkLogic).Name)
				{
					typeNames.Add(type.FullName);
				}
			}
			typeNames.Sort();

			selectedIndex = EditorGUILayout.Popup("脚本名", selectedIndex, typeNames.ToArray(), GUILayout.Width(250));
			TilemapHelper.TargetMapData.ScriptName = typeNames[selectedIndex];

			GUILayout.EndVertical();
		}

		private void MapDataRight()
		{
			GUILayout.BeginVertical();

			string curMapStr = CurEditMapName == "" ? "无，请选择编辑的地图块" : CurEditMapName;
			GUILayout.Label("当前编辑的地图：" + curMapStr, GUILayout.ExpandWidth(true));

			IsDrawLine = GUILayout.Toggle(IsDrawLine, "是否显示地图边界");

			GUILayout.EndVertical();
		}

		private void SearchTileContent()
		{
			GUILayout.Space(20);
			if (TilemapHelper.TargetMapData.TileInfoList == null)
			{
				GUILayout.Label("Tile TotalCount : 0");
			}
			else
			{
				GUILayout.Label("Tile TotalCount : " + TilemapHelper.TargetMapData.TileInfoList.Count);
			}
			GUILayout.Label("Tile SearchCount : " + searchCount);
			if (searchText == null)
			{
				searchText = "";
			}
			GUILayout.BeginHorizontal();
			GUILayout.Label("请输入搜索关键字:", GUILayout.Width(120));
			searchText = GUILayout.TextField(searchText, GUILayout.ExpandWidth(true));
			if (GUILayout.Button("清空输入", GUILayout.Width(80)))
			{
				searchText = "";
				GUI.changed = true;
				GUIUtility.keyboardControl = 0;
			}
			GUILayout.EndHorizontal();

			scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
			int tempCount = 0;
			if (TilemapHelper.TargetMapData.TileInfoList != null && TilemapHelper.TargetMapData.TileInfoList.Count != 0)
			{
				for (int i = 0; i < TilemapHelper.TargetMapData.TileInfoList.Count; i++)
				{
					CherryTile tile = (CherryTile)TilemapHelper.TargetMapData.TileInfoList[i].Tile;
					if (tile != null)
					{
						m_StringBuilder.Clear();
						string tempStr = m_StringBuilder.Append("【").Append(i.ToString("0000")).Append("】     Pos:").Append(TilemapHelper.TargetMapData.TileInfoList[i].Pos.ToString()).Append("      Tile:").Append(tile.ToString()).ToString();
						if (tempStr.ToLower().Contains(searchText.ToLower()))
						{
							tempCount++;
							GUILayout.Label(tempStr);
						}
					}
				}
			}
			searchCount = tempCount;
			EditorGUILayout.EndScrollView();
		}
		#endregion

		private void OnInspectorUpdate()
		{
			this.Repaint();
		}

		/// <summary>
		/// 创建并编辑当前选择的Tilemap的地图信息。
		/// </summary>
		/// <param name="data"></param>
		private void CreateCurTileMap(TilemapData data)
		{
			//上次编辑的地图成为预览状态，需要创建或更新静态TilemapData数据
			if (LoadedMapDatas.TryGetValue(CurEditMapName, out var lastEditData))
			{
				//更新
				LoadedMapDatas.Remove(CurEditMapName);
				LoadedMapDatas.Add(CurEditMapName, TilemapHelper.CloneMapData(m_LastEditData));
			}
			else
			{
				//添加
				if (m_LastEditData != null)
				{
					LoadedMapDatas.Add(CurEditMapName, TilemapHelper.CloneMapData(m_LastEditData));
				}
			}

			//Load数据，更新TargetMapData为当前data
			TilemapHelper.SetTargetData(data);
			string name = GetTilemapFullName(TilemapHelper.TargetMapData);
			//判断是否加载了同一块地图
			if (!LoadedMapDatas.TryGetValue(name, out var loadedData))
			{
				CreateTilemapObj(name, data);
			}

			//编辑该Tilemap
			CurEditMapName = name;
			m_LastEditData = TilemapHelper.CloneMapData(data);
			//LoadedMapDatas.Add(name, data);
			Selection.activeObject = GameObject.Find(name);
		}

		/// <summary>
		/// 创建预览地图块
		/// </summary>
		/// <param name="data"></param>
		private void CreatePreviewMap(TilemapData data)
		{
			string name = GetTilemapFullName(data);
			if (CurEditMapName == name)
			{
				EditorUtility.DisplayDialog("提示", string.Format("正在编辑想要预览的地图！地图Name：{0}。", name), "确认");
				return;
			}

			//判断是否加载了同一块地图
			if (LoadedMapDatas.TryGetValue(name, out var loadedData))
			{
				EditorUtility.DisplayDialog("提示", string.Format("正在预览{0}地图。", name), "确认");
			}
			else
			{
				CreateTilemapObj(name, data);
				LoadedMapDatas.Add(name, data);
			}
		}

		private void CreateTilemapObj(string name, TilemapData data)
		{
			GameObject grid = GameObject.Find("Grid");
			//没有grid，则create grid
			if (grid == null)
			{
				grid = new GameObject("Grid");
				grid.transform.position = new Vector3(-0.5f, -0.5f, 0);
				grid.AddComponent<Grid>();
			}

			GameObject go = GameObject.Find(name);
			if (go != null)
			{
				DestroyImmediate(go);
			}
			GameObject tilemap = new GameObject(name);
			tilemap.transform.SetParent(grid.transform);
			tilemap.transform.localPosition = Vector3.zero;
			Tilemap map = tilemap.AddComponent<Tilemap>();
			TilemapRenderer render = tilemap.AddComponent<TilemapRenderer>();
			render.sortOrder = (TilemapRenderer.SortOrder)data.SortOrderIndex;
			render.sortingOrder = data.OrderInLayer;
			render.sortingLayerName = SortingLayer.layers[data.SortingLayerIndex].name;
			TilemapHelper.SetTileMap(map, data);
		}

		private string GetTilemapFullName(TilemapData data)
		{
			return string.Format("{0} - {1}", data.TilemapName, data.TilemapGroupName);
		}

		private void SettingContent()
		{

		}

		private void AboutContent()
		{

		}

		private void CreateTileMap()
		{

		}

		
	}
}
