﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Cherry.Tilemaps;
using UnityEngine.Tilemaps;
using System.Text;

namespace Cherry.Editor.Tilemaps
{
    /// <summary>
    /// 用于在Inspector面板显示Tile的详细信息
    /// </summary>
    [CustomEditor(typeof(MapBrush))]
    public class MapBrushEditor : UnityEditor.Tilemaps.GridBrushEditor
    {
		//TODO 研究GridBrushEditor
		private MapBrushEditor prefabBrush { get { return target as MapBrushEditor; } }

		private StringBuilder m_StringBuilder;

		public override void PaintPreview(GridLayout grid, GameObject brushTarget, Vector3Int position)
		{
			base.PaintPreview(grid, brushTarget, position);
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			m_StringBuilder = new StringBuilder(128);
		}

		private void OnDestroy()
		{
			m_StringBuilder.Clear();
		}

		public override void OnSelectionInspectorGUI()
		{
			base.OnSelectionInspectorGUI();
			MapBrush brush = target as MapBrush;
			if (brush.cells.Length == 1)
			{
				CherryTile tile = brush.cells[0].tile as CherryTile;
				if (tile != null)
				{
					if (EditorGUILayout.Toggle("IsNpc", tile.IsNpc))
					{
						EditorGUILayout.DelayedTextField("NpcName", tile.NpcName);
					}
					if (EditorGUILayout.Toggle("IsMonsterCreator", tile.IsMonsterCreator))
					{
						EditorGUILayout.DelayedTextField("MonsterName", tile.MonsterName);
					}
				}
			}
		}

		public override void OnPaintInspectorGUI()
		{
			GUILayout.Label("https://gitee.com/game-inventor/cherry-2-d--demo");
		}

		public override void OnPaintSceneGUI(GridLayout grid, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing)
		{
			base.OnPaintSceneGUI(grid, brushTarget, position, tool, executing);
			//当前grid的坐标
			Handles.Label(grid.CellToWorld(new Vector3Int(position.x, position.y, position.z)), ToStringVector3Int(new Vector3Int(position.x, position.y, position.z)));

			PaintMapOnScene(TilemapHelper.TargetMapData, Color.red);
			foreach (var data in ChunkEditor.LoadedMapDatas)
			{
				if (data.Key != ChunkEditor.CurEditMapName)
				{
					PaintMapOnScene(data.Value, Color.green);
				}
			}
		}


		private void PaintMapOnScene(TilemapData data, Color color)
		{
			if (ChunkEditor.IsDrawLine)
			{
				//左下角，左上角，右下角，右上角
				Vector3 pos1 = new Vector3(data.ChunkBeginPosX - 0.5f, data.ChunkBeginPosY - 0.5f);
				Vector3 pos2 = new Vector3(data.ChunkBeginPosX - 0.5f, data.ChunkBeginPosY + data.ChunkHeight - 0.5f);
				Vector3 pos3 = new Vector3(data.ChunkBeginPosX + data.ChunkWidth - 0.5f, data.ChunkBeginPosY - 0.5f);
				Vector3 pos4 = new Vector3(data.ChunkBeginPosX + data.ChunkWidth - 0.5f, data.ChunkBeginPosY + data.ChunkHeight - 0.5f);
				Debug.DrawLine(pos1, pos2, color);
				Debug.DrawLine(pos1, pos3, color);
				Debug.DrawLine(pos2, pos4, color);
				Debug.DrawLine(pos3, pos4, color);
				Handles.Label(pos1, ToStringVector3Int(new Vector3Int((int)data.ChunkBeginPosX, (int)data.ChunkBeginPosY, 0)));
				Handles.Label(pos2, ToStringVector3Int(new Vector3Int((int)data.ChunkBeginPosX, (int)(data.ChunkBeginPosY + data.ChunkHeight - 1), 0)));
				Handles.Label(pos3, ToStringVector3Int(new Vector3Int((int)(data.ChunkBeginPosX + data.ChunkWidth - 1), (int)data.ChunkBeginPosY, 0)));
				Handles.Label(pos4, ToStringVector3Int(new Vector3Int((int)(data.ChunkBeginPosX + data.ChunkWidth - 1), (int)(data.ChunkBeginPosY + data.ChunkHeight - 1), 0)));
			}
		}

		private string ToStringVector3Int(Vector3Int mInt)
		{
			m_StringBuilder.Clear();
			m_StringBuilder.Append("(");
			m_StringBuilder.Append(mInt.x);
			m_StringBuilder.Append(",");
			m_StringBuilder.Append(mInt.y.ToString());
			m_StringBuilder.Append(")");
			return m_StringBuilder.ToString();
		}
	}
}
